import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-current-date-time',
  templateUrl: './current-date-time.component.html',
  styleUrls: ['./current-date-time.component.scss'],
  providers: [DatePipe]
})
export class CurrentDateTimeComponent implements OnInit {
  public now: Date = new Date();
  currentDate:string='';
  options:any = { weekday: 'long', month: 'long', day: 'numeric' };
 // options:any = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  constructor(private datePipe: DatePipe) {
    this.now = new Date();
    this.currentDate = this.now.toLocaleDateString("en-US", this.options);
  }

  ngOnInit(): void {
  }

}
